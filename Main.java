import ccd.steganography.ImageSteganography;
import ccd.steganography.Steganography;
import ccd.steganography.image.lsb.SpatialLSB;

import java.awt.image.BufferedImage;
import java.io.File;

public class Main {

    public static void main(String[] args){
        //TODO : add method
        File imgFile = new File
                ("C:\\Users\\dcunh\\IdeaProjects\\Steganography\\src\\ssm.png");
        File data = new File
                ("C:\\Users\\dcunh\\IdeaProjects\\Steganography\\src\\data.txt");
        File outImg = new File
                ("C:\\Users\\dcunh\\IdeaProjects\\Steganography\\src\\image.png");
        File outDataPath = new File
                ("C:\\Users\\dcunh\\IdeaProjects\\Steganography\\src\\");
        String homePath =
                "C:\\Users\\dcunh\\IdeaProjects\\Steganography\\src\\";

        try{
            Steganography l1 = new SpatialLSB(100);
            ImageSteganography.saveImage((BufferedImage) l1.encode(imgFile,data), outImg);
            l1.decode(outImg, outDataPath);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
}
